from setuptools import setup

setup(
    name='nb_opensource_common',
    version='1.0',
    description='Module for common code that has utility functions.',
    author='Haris Aziz',
    author_email='haris.aziz@northbaysolutions.net',
    packages=['nb_opensource_common'],  # same as name
    install_requires=['wheel'],# external packages as dependencies
    python_requires='>=3.6',
)
