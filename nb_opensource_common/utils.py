__author__ = 'haris.aziz'


class Utils:
    """
    Common Util Class
    """

    @staticmethod
    def get_object_from_list(object_list, name):
        """
        Get configuration for Auroa and Snowflake
        :param object_list:
        :param name: config file path
        :return: return object
        """
        for obj in object_list:
            if obj['name'] == name:
                return obj
        return None
