__author__ = 'ali.hussain'

import psycopg2
from nb_opensource_common.aws_utils import AWSSSMUtils
import logging


class AuroraConnectionService:
    """
    This class is responsible to Aurora connection based on the config params
    """
    def __init__(self, config: dict):
        """
        Constructor
        :param config: dictionary having all the config param required to make connection call
        """
        try:
            ssm_utils = AWSSSMUtils()
            self.__connection = psycopg2.connect(
                host=config['connection']['host'],
                port=config['connection']['port'],
                database=config['connection']['database'],
                user=config['connection']['user_name'],
                password=ssm_utils.get_value(config['connection']['password_ssm_key']),
            )
            logging.info('Aurora connection successful')
        except Exception as exp:
            logging.error(f'Aurora connection failed. Check credentials {exp}')
            raise Exception(f'Aurora connection failed. Check credentials {exp}')

    def get_aurora_connection(self):
        """
        This function would return the connection object, creating in constructor
        :return: Aurora connection object
        """
        return self.__connection

    def close_connection(self):
        """
        Closes the connection
        :return:
        """
        self.__connection.close()

    def execute_function(self, name, params: tuple):
        """

        :param name:
        :param params:
        :return:
        """
        cursor = self.__connection.cursor()
        cursor.callproc(name, params)
        function_result = cursor.fetchone()[0]
        cursor.close()
        return function_result

    def fetch_query_result(self, queries: dict):
        """

        :param queries:
        :return:
        """
        query_results = None
        cursor = self.__connection.cursor()
        for query in queries:
            cursor.execute(query)
            query_results = cursor.fetchall()
        cursor.close()
        return query_results
