__author__ = 'haris.aziz'

import snowflake.connector as sf_connector
from nb_opensource_common.aws_utils import AWSSSMUtils
import time
import logging


class SnowflakeConnectionService:
    def __init__(self, config):
        self.__connection = None
        try:
            ssm_utils = AWSSSMUtils()
            if 'authenticator' in config:
                self.__connection = sf_connector.connect(
                    user=config['user_name'],
                    password=ssm_utils.get_value(config['password_ssm_key']),
                    account=config['account'],
                    authenticator=config['authenticator']
                )
            else:
                self.__connection = sf_connector.connect(
                    user=config['user_name'],
                    password=ssm_utils.get_value(config['password_ssm_key']),
                    account=config['account']
                )

        except Exception as exp:
            logging.error(f'Snowflake connection failed. Check credentials {exp}')
            raise Exception(f'Snowflake connection failed. Check credentials {exp}')

    @staticmethod
    def get_copy_queries(tables, stage):
        copy_queries = []
        for table in tables:
            partitions = tables[table]
            for partition in partitions:
                location = partitions[partition]
                copy_query = "copy into " + table + " from @" + stage + "/" + location + ";"
                print(copy_query)
                copy_queries.append(copy_query)
        return copy_queries

    def get_snowflake_tables_from_schema(self, schema):
        query = "SELECT table_name FROM information_schema.tables where table_schema = upper('" + schema + "');"
        cur = self.__connection.cursor()
        cur.execute(query)
        table_names = []
        for (name) in cur:
            table_names.append(name[0])
        return table_names

    def snowflake_usage_setup(self, config):
        """
        This function would setup USAGE for roles, schema, Database or Warehouse
        :param config:
        :return:
        """
        logging.debug('Setting up usage of roles, warehouse, database and schema')
        if "role" in config:
            self.execute_sql(f'USE ROLE {config["role"]};')

        if "warehouse" in config:
            self.execute_sql(f'USE WAREHOUSE {config["warehouse"]};')

        if "database" in config:
            self.execute_sql(f'USE DATABASE {config["database"]};')

        if "schema" in config:
            self.execute_sql(f'USE SCHEMA {config["schema"]};')

    @staticmethod
    def load_data(cur):
        batch_count = 0
        row_count = 0
        for df in cur.fetch_pandas_batches():
            batch_count += 1
            row_count += df.shape[0]
            del df
        print("batches: " + str(batch_count))
        print("Row count: " + str(row_count))
        return row_count

    def run_snowflake_query(self, query):
        start_time = time.time()

        cur = self.__connection.cursor()
        cur.execute(query)
        size = self.load_data(cur)

        total_time = time.time() - start_time
        return size, total_time

    def disable_snowflake_cache(self, sql):
        self.execute_sql("ALTER SESSION SET USE_CACHED_RESULT = FALSE")

    def execute_sql(self, sql):
        cursor = self.__connection.cursor()
        cursor.execute(sql)
        cursor.close()

    def execute_procedure(self, sql):
        cursor = self.__connection.cursor()
        cursor.execute(sql)
        procedure_result = cursor.fetchone()[0].replace("\\n", " ")
        cursor.close()
        return procedure_result

    def close_connection(self):
        self.__connection.close()
