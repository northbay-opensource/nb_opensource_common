import boto3
import logging
import io
import pandas as pd
import pythena


class AWSS3Utils:
    """
    This class is responsible to interact with s3 through api
    """
    s3_max_keys = 1000

    def __init__(self):
        """
        Constructor
        initialize boto3 client for s3
        :return
        """
        self.s3_client = boto3.client('s3')
        self.s3_resource = boto3.resource('s3')

    def s3_get_objects(self, bucket, prefix, next_token):
        """
        S3 api to fetch objects from s3 bucket or from next token if tables are greater than 1000
        :param bucket:
        :param prefix:
        :param next_token:
        :return:
        """
        if not next_token:
            res = self.s3_client.list_objects_v2(
                Bucket=bucket,
                MaxKeys=AWSS3Utils.s3_max_keys,
                Prefix=prefix,
                StartAfter=prefix
            )
        else:
            res = self.s3_client.list_objects_v2(
                Bucket=bucket,
                MaxKeys=AWSS3Utils.s3_max_keys,
                Prefix=prefix,
                StartAfter=prefix,
                ContinuationToken=next_token
            )
        return res

    def get_s3_objects(self, bucket, prefix):
        """
        Return all the objects from given S3 bucket and prefix
        :param bucket:
        :param prefix:
        :return:
        """
        token = None
        table_list = []
        while True:
            res = self.s3_get_objects(bucket, prefix, token)
            table_list += res['Contents']
            if "NextContinuationToken" not in res:
                break
            token = res['NextContinuationToken']
        return table_list

    def s3_get_object(self, bucket, file):
        """
        Return contents of s3 file
        :param bucket:
        :param file:
        :return:
        """
        obj = self.s3_resource.Object(bucket, file)
        body = obj.get()['Body'].read().decode("utf-8")
        return body

    def copy_object_source_to_destination(self, source_bucket, source_key, target_bucket, target_key):
        """
        Copy object from one s3 location to another s3 location
        :param source_bucket:
        :param source_key:
        :param target_bucket:
        :param target_key:
        :return:
        """
        try:
            copy_source = {
                'Bucket': source_bucket,
                'Key': source_key
            }
            self.s3_resource.meta.client.copy(copy_source, target_bucket, target_key)
        except Exception as err:
            raise Exception(f"{copy_source}: {err}")

    def get_header_and_types(self, bucket, prefix, delimiter):
        """
        Sample s3 csv file data through pandas and fetch data types and header information
        :param bucket:
        :param prefix:
        :param delimiter
        :return: dictionary
        """

        obj = self.s3_client.get_object(Bucket=bucket,
                                        Key=prefix)
        df = pd.read_csv(io.BytesIO(obj['Body'].read()), nrows=1000, delimiter=delimiter)
        type_mapping = self.get_pandas_to_athena_mapping()
        header_dic = {}
        for name, data_type in df.dtypes.iteritems():
            header_dic[name] = type_mapping[str(data_type)]
        return header_dic

    @staticmethod
    def get_pandas_to_athena_mapping():
        """
        returns pandas to athena data type mapping
        :return: dictionary
        """
        data_type_dict = {
            'object': 'string',
            'int64': 'bigint',
            'float64': 'double',
            'bool': 'boolean',
            'datetime64': 'timestamp'
        }
        return data_type_dict


class AWSAthenaUtils:
    """
    This class is responsible to interact with ATHENA through boto3 api
    """
    def __init__(self):
        """
        Constructor
        initialize boto3 client for athena
        :return
        """

        self.athena_client = boto3.client('athena')

    def get_athena_query_execution(self, execution_id):
        """
        athena api to get query statistics
        :param execution_id:
        :return:
        """
        response = self.athena_client.get_query_execution(
            QueryExecutionId=execution_id
        )
        return response

    @staticmethod
    def run_athena_query(query, db_name, workgroup, region,  output_location):
        """
        run sync athena query
        :param query:
        :param db_name:
        :param workgroup:
        :param region:
        :param output_location:
        :return:
        """
        pythena_client = pythena.Athena(database=db_name, region=region)
        data_frame, execution_id = pythena_client.execute(query=query, workgroup=workgroup,
                                                          s3_output_url=output_location)
        return execution_id, data_frame.shape[0]


class AWSGlueUtils:
    """
    This class is responsible to interact with GLUE through boto3 api
    """
    glue_tables_max_keys = 1000

    def __init__(self):
        """
        Constructor
        initialize boto3 client for glue
        :return
        """
        self.glue_client = boto3.client('glue')

    def get_glue_tables(self, database, next_token):
        """
        Call Glue api to fetch tables from Glue database or from next token if tables are greater than 1000
        :param database:
        :param next_token:
        :return:
        """
        if not next_token:
            res = self.glue_client.get_tables(
                DatabaseName=database,
                MaxResults=AWSGlueUtils.glue_tables_max_keys
            )
        else:
            res = self.glue_client.get_tables(
                DatabaseName=database,
                MaxResults=AWSGlueUtils.glue_tables_max_keys,
                NextToken=next_token
            )
        return res

    def get_tables_in_database(self, database):
        """
        Return all the tables from given Glue database
        :param database:
        :return:
        """

        token = None
        table_list = []
        while True:
            res = self.get_glue_tables(database, token)
            table_list += res['TableList']
            if "NextToken" not in res:
                break
            token = res['NextToken']
        return table_list

    def create_glue_database(self, db_name):
        """
        Create a glue database
        :param db_name:
        :return:
        """
        response = self.glue_client.create_database(
            DatabaseInput={
                'Name': db_name
            }
        )
        logging.info(f'{response}')

    def check_glue_database(self, db_name):
        """
        Check if Glue database exist or not
        :param db_name:
        :return: True/False
        """
        try:
            response = self.glue_client.get_database(
                Name=db_name
            )
            logging.info(f'{response}')
            return True
        except Exception as err:
            logging.info(f'{err}')
            return False

    def create_glue_table(self, db_name, table_name, column_list, partition_column, location, delimiter):
        """
        Create glue table in specified database
        :param db_name:
        :param table_name:
        :param column_list:
        :param partition_column:
        :param location:
        :param delimiter:
        :return:
        """
        response = self.glue_client.create_table(
            DatabaseName=db_name,
            TableInput={
                'Name': table_name,
                'StorageDescriptor': {
                    'Columns': column_list,
                    'Location': location,
                    'InputFormat': 'org.apache.hadoop.mapred.TextInputFormat',
                    'OutputFormat': 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat',
                    'SerdeInfo': {
                        'SerializationLibrary': 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe',
                        'Parameters': {
                            'field.delim': delimiter
                        }
                    }
                },
                "TableType": "EXTERNAL_TABLE",
                'PartitionKeys': [
                    {
                        'Name': partition_column,
                        'Type': 'string'
                    }
                ],
                'Parameters': {
                    'skip.header.line.count': '1',
                    'classification': 'csv',
                    'columnsOrdered': 'true',
                    'delimiter': delimiter,
                    'typeOfData': 'file',
                    'areColumnsQuoted': 'false'
                }
            }
        )
        logging.info(f'{response}')

    def create_partition(self, db_name, table_name, column_list, partition, location, delimiter):
        """
        call create partition api by giving s3 location to load partition in glue table

        :param db_name:
        :param table_name:
        :param column_list:
        :param partition:
        :param location:
        :param delimiter:
        :return:
        """
        response = self.glue_client.create_partition(
            DatabaseName=db_name,
            TableName=table_name,
            PartitionInput={
                'Values': [
                    partition
                ],
                'StorageDescriptor': {
                    'Columns': column_list,
                    'Location': location,
                    'InputFormat': 'org.apache.hadoop.mapred.TextInputFormat',
                    'OutputFormat': 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat',
                    'SerdeInfo': {
                        'SerializationLibrary': 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe',
                        'Parameters': {
                            'field.delim': delimiter
                        }
                    },
                },
                'Parameters': {
                    'skip.header.line.count': '1',
                    'classification': 'csv',
                    'columnsOrdered': 'true',
                    'delimiter': delimiter,
                    'typeOfData': 'file',
                    'areColumnsQuoted': 'false'
                }
            }
        )
        logging.info(f'{response}')


class AWSSSMUtils:
    def __init__(self):
        self.SSM_CLIENT = boto3.client('ssm')

    @staticmethod
    def __get_value_from_parameter(parameter: dict):
        """
        Helper method to pull the value from the parameter dict
        :param parameter value
        :return:
        """
        return parameter['Parameter']['Value']

    def get_value(self, ssm_key):
        """
        Get an plaintext value from the parameter store
        :param ssm_key:
        :return:
        """
        return self.__get_value_from_parameter(self.SSM_CLIENT.get_parameter(Name=ssm_key))

    def get_value_decrypted(self, ssm_key):
        """
        Return a decrypted value from the parameter store
        :param ssm_key:
        :return:
        """
        return self.__get_value_from_parameter(self.SSM_CLIENT.get_parameter(Name=ssm_key, WithDecryption=True))
