__author__ = 'ali.hussain'

import json
from pathlib import Path


class ConfigLoader:
    """
    Load Configuration class
    """

    @staticmethod
    def get_configuration(config_file: str):
        """
        Get configuration for Auroa and Snowflake
        :param config_file:
        :return: config json object
        """
        config = None
        try:
            base_path = Path(__file__).parent
            file_path = (base_path / config_file).resolve()
            config = json.loads(open(file_path).read())
        except Exception as exp:
            print(f"File Not Found {exp}")

        return config
